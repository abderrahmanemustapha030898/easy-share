# easy-share
> a tool to send your files to a one or multiple servers you can use a ui interface or a command line for it 
## Todo

- [X] connection to server
- [X] send file to a server
- [X] ui interface
- [X] execute .sh files
- [X] connect to multiple servers
- [X] send files to multi servers
- [X] add web sockets
- [X] display messages and notifications to the user
- [ ] user can access this tool using commnad line
- [ ] get results files
- [ ] make a better ui

##  How to use
### Clone
> Clone this repo to your local machine using :
```shell

```
### Instalation 

```shell
pip install -r requirements.txt
```

### Run 

```shell
python app.py
```