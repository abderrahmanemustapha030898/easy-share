
class ServerForm():
    pass


def allowed_file(filename):
    ALLOWED_EXTENSIONS = {'.tar', ".gz", '.zip', ".rar"}
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS