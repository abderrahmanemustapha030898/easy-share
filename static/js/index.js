/// HELPERS

add_to_form = (formdata, input_list)=>{
    Array.prototype.forEach.call(input_list, element=>{
        formdata.append(element.name, element.value)
    })
}


/// helpers end


//////////////////////// it about adding new server input //////////////////
// add an onclick listener to add server button 
// because we need to add the cloned element to the dom and exactly the main-form
document.getElementById('add-server').addEventListener('click', event=>{
    // get the last child of the main form the row 
    // which contains the username, host , password, path
    form = document.getElementById("form-rows")
    all_rows = form.getElementsByClassName('form-row')
    element = all_rows[all_rows.length - 1]

    // clone the first row
    element2 = element.cloneNode(true);
    // get the header of the new  element and edit the text 
   header = element2.getElementsByTagName('h5')[0]
   header.innerText = `Server ${all_rows.length +1 }`

   form.appendChild(element2) // add to form and we are done
})
//////////////////////// the end is here //////////////////

// user interface logger
logger =  document.getElementById("logger")
// first lets give our user the ability to clear logger
document.getElementById('clear-log').addEventListener('click', event=>{
    logger.innerText = ""
})

/////////////////////////// socket io part ///////////////////////////////:


var socket = io();
socket.on('connect', function() {
    logger.innerText += 'socket connected... \n'
});


//// send to server and verify data 
document.getElementById('submite-connection').addEventListener('click', event=>{
    event.preventDefault()
    host = document.getElementsByName("hostname")
    password = document.getElementsByName("hostpassword")
    path = document.getElementsByName("path")
    keypath = document.getElementsByName("keypath")
    formData = new FormData()
    formData.append('file', document.getElementById('file').files[0])
    add_to_form(formData, host)
    add_to_form(formData, password)
    add_to_form(formData, path)
    add_to_form(formData, keypath)
    fetch("/",{
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    body: formData
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)
        logger.innerText += data["data"] + "... \n"
        socket.emit('client-connect', "start a new server connection")
    });
   ; 
})



socket.on('server_try_connect', function(data) {
    console.log(data['data'])
    logger.innerText += data['data']+ ".... \n"
});

socket.on('server-connected', function(data) {
    console.log(data)
    logger.innerText += data['data']+ ".... \n"
});

socket.on('server-connected-establish', function(data) {
    console.log(data)
    logger.innerText += data['data']+ ".... \n"
});

socket.on('server-connected-success', function(data) {
    console.log(data)
    logger.innerText += data['data']+ ".... \n"
});

socket.on('sending-files', function(data) {
    console.log(data)
    logger.innerText += data['data']+ ".... \n"
});

socket.on('files-sent', function(data) {
    console.log(data)
    logger.innerText += data['data']+ ".... \n"
});

socket.on('files-extract', function(data) {
    console.log(data)
    logger.innerText += data['data'] + ".... \n"
});

socket.on('files-extract-success', function(data) {
    console.log(data)
   console.log(data['data'])  
   data['data']['result'].forEach(val=>{
    console.log(val)
    logger.innerText += val + ".... \n"
})
   
    
});

socket.on('list-dir', function(data) {
    console.log(data)
    logger.innerText += "this directory contains :.... \n"
    data['data']['result'].forEach(val=>{
        console.log(val)
        logger.innerText += val + ".... \n"
    })
   
});

socket.on('connection-closed', function(data) {
    console.log(data)

    logger.innerText += data['data'] + ".... \n"
    
});