from flask import Flask, url_for, request, render_template, jsonify


from flask_socketio import SocketIO, emit
from itertools import zip_longest


import os

# activate eventlet
import eventlet
eventlet.monkey_patch()


from utils import Connection
from validators import allowed_file
#init the flask app
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = os.path.join(dir_path,'/media')

app = Flask(__name__,static_url_path='/static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
socketio = SocketIO(app, async_mode="eventlet")

data = {}


@socketio.on("connect")
def handle_connect():
    
    print("socket connected to server")
    emit('server_try_connect', { 'data' : 'connect successfuly waiting for your inputs'})



@socketio.on('client-connect')
def client_connected(message):
    global data
    
    HOST = data["host"]
   

    PASSWORD = data['password']
    PATH = data['path']
    KEY = data['key']
    FILE = data['file']
    
    if data:
        for (host, password, path, key) in zip_longest(HOST, PASSWORD, PATH, KEY):
            hostusername , host  = host.split('@')
            print("host username ", hostusername )
            print("host  ", host)
            #connect to the ssh server
            emit('server_try_connect', {'data' : str('trying to connect to server '+host)})
            client = Connection(host=host, username=hostusername, password=password, keypath=key)
            emit('server-connected', {'data' :  'client connected'})
            emit('server-connected-establish',{'data' : str('establishing connection to  '+host)})
            client.connect()
            emit('server-connected-success', {'data' :  'client connected'})
            #sending files.....
            emit('sending-files',  {'data' : "sending file this may take some time please waite"})
            client.send_files_path(os.path.join(app.config['UPLOAD_FOLDER'], FILE), FILE, path)
            emit('files-sent', {'data' : "file sent successfully"})
            emit('files-extract',{'data' : "extracting files this may take some time please waite"})
            emit('files-extract-success', {'data' : client.extract(path, file_name=FILE)})
        
            #check if the file existe / and command lines
            client.excute_command("ls")
            emit('list-dir',  {'data' : client.check_command()})
            client.excute_command("/"+path+"/cert_install.sh")
            
            client.excute_command("/"+path+"/cert_benchmark.sh")
           
            
            

            client.close_connection()
            emit('connection-closed', {'data' :  ('connection closed')})
  
@app.route('/',methods=['GET', 'POST'])
def home(): 

    if request.method == "POST":
        #get input from form
        HOST = request.form.getlist('hostname')
        PASSWORD = request.form.getlist('hostpassword')
        PATH = request.form.getlist('path')
        KEY = request.form.getlist('keypath')
        FILE = request.files.get('file')
        
        
        
        if FILE :
            FILE.save(os.path.join(app.config['UPLOAD_FOLDER'], FILE.filename))
            global data
            data = {"host": HOST,  "password":PASSWORD, "path":PATH, "file": FILE.filename, "key":KEY  }
            return jsonify({"data": "data sent successfully"})
        else:
            return jsonify({"data": "wrong credentials"})
       
    return render_template("main.html")




if __name__ == '__main__':
  socketio.run(app, debug=True)