#contains helper functions and classes


import paramiko
from paramiko.py3compat import b, u, decodebytes
import os



#connect to ssh server
class Connection:
    def __init__(self, host, username, password, keypath):
        self.stdin = ""
        self.stdout = ""
        self.stderr = ""
        self.host =host
        self.username = username
        self.password = password
        self.key  = paramiko.RSAKey(filename=keypath, password=password) if keypath else ""
        print("my key is ", self.key)
        self.client = paramiko.SSHClient()
        keys = self.client.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
        print(keys)
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    # connect to ssh server
    def connect(self): 
        UseGSSAPI = (
            paramiko.GSS_AUTH_AVAILABLE
        )  # enable "gssapi-with-mic" authentication, if supported by your python installation
        DoGSSAPIKeyExchange = (
            paramiko.GSS_AUTH_AVAILABLE
        )  # enable "gssapi-kex" key exchange, if supported by your python installation 

        print("UseGSSAPI ", UseGSSAPI)
        print("DoGSSAPIKeyExchange ", DoGSSAPIKeyExchange)
        
        
        if not self.password:
            print("there is not password")
            self.client.connect(self.host,username=self.username , gss_auth=UseGSSAPI,
                gss_kex=DoGSSAPIKeyExchange,timeout=200)
        else:
            self.client.connect(self.host, username=self.username, password=self.password,  timeout=200)

    def path_exists(self,sftp,path):
        try:
            sftp.stat(path)
            return True
        except Exception as e:
            return False
            
    
    #here we are creating a file in the server and checking the command line
    def create_and_navigate_topath(self,sftp,file_path_inserver):
            if self.path_exists(sftp,file_path_inserver):
                sftp.chdir(file_path_inserver)
            else:
                for file__ in file_path_inserver.split('/') : 
                    sftp.mkdir(file__)  # Create remote_path
                    sftp.chdir(file__)

    

    #send a storage file
    def send_filestorage(self, file_storage, file_in_server, file_path_inserver):
        sftp = self.client.open_sftp()

        # create a file path if the we have a path from the user
        if file_path_inserver:
            self.create_and_navigate_topath(sftp,file_path_inserver)
        sftp.putfo(file_storage, file_in_server)
        sftp.close()
        

    #send a file path
    def send_files_path(self, local_file_path, file_in_server, file_path_inserver):
        sftp = self.client.open_sftp()
        # create a file path if the we have a path from the user
        if file_path_inserver:
            self.create_and_navigate_topath(sftp,file_path_inserver)
        sftp.put(local_file_path, file_in_server)
        sftp.close()

    # extract zip, tar , rar files
    def extract(self, path, file_name):
        name, extension = os.path.splitext(str(file_name))
        def switcher(argument):
            switch = {
                ".tar" : "tar xvf",
                ".zip" : "unzip",
                ".rar" : "unrar",
                ".gz" : "tar -xf "
            }
            return switch[argument]
        path = path if path else name
        command =  switcher(extension) + " " + path+"/"+""+ file_name + " -d "+path
        self.excute_command(command)
        return self.check_command()

    def run_sh_scripts(self, cmd):
        self.excute_command(cmd)
        
    def excute_command(self, cmd):
        print("im tryning to execute this", cmd)
        self.stdin, self.stdout, self.stderr = self.client.exec_command(cmd)

    def check_command(self):
        result = []
        
        for line in self.stdout.readlines():
            print('....', line.strip('\n') )
            result.append(line.strip('\n'))

        for line in self.stderr.readlines():
            print('....', line.strip('\n') )
            result.append(line.strip('\n'))
        return {"result" : result}



    def close_connection(self):
        self.client.close()
    
